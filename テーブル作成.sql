﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;

CREATE TABLE user
    (
    id serial primary key auto_increment,
    login_id varchar(255) unique not null,
    name varchar(255) not null,
    birth_date date not null,
    password varchar(255) not null,
    create_date datetime not null,
    update_date datetime not null
    );
    
insert into user values
    (
    1,
    'admin',
    '管理者',
    0101,
    'password',
    now(),
    now()
    );

